# Ansible k8s_raw bug minimal example

This is part of the issue https://github.com/ansible/ansible/issues/47081 regarding Ansible version 2.7.0.

## Setup

`minikube start --kubernetes-version v1.12.1`

## Running

`ansible-playbook k8s_raw_cert_manager_ansible_bug.yml``

### Current output

```
manzo@EVA ..8s_raw_cert_manager_ansible_bug_report (git)-[master] % ansible-playbook k8s_raw_cert_manager_ansible_bug.yml
 [WARNING]: Unable to parse /etc/ansible/hosts as an inventory source

 [WARNING]: No inventory was parsed, only implicit localhost is available

 [WARNING]: provided hosts list is empty, only localhost is available. Note that the implicit localhost does not match 'all'


PLAY [127.0.0.1] **********************************************************************************************************************************************************************************************************

TASK [Gathering Facts] ****************************************************************************************************************************************************************************************************
ok: [127.0.0.1]

TASK [k8s_raw] ************************************************************************************************************************************************************************************************************
changed: [127.0.0.1]

TASK [k8s_raw] ************************************************************************************************************************************************************************************************************
fatal: [127.0.0.1]: FAILED! => {"changed": false, "module_stderr": "/home/manzo/.ansible/tmp/ansible-tmp-1539882988.6375449-224210235286945/AnsiballZ__k8s_raw.py:17: DeprecationWarning: the imp module is deprecated in f
see the module's documentation for alternative uses\n  import imp\nTraceback (most recent call last):\n  File \"/home/manzo/.ansible/tmp/ansible-tmp-1539882988.6375449-224210235286945/AnsiballZ__k8s_raw.py\", line 113,
siballz_main()\n  File \"/home/manzo/.ansible/tmp/ansible-tmp-1539882988.6375449-224210235286945/AnsiballZ__k8s_raw.py\", line 105, in _ansiballz_main\n    invoke_module(zipped_mod, temp_path, ANSIBALLZ_PARAMS)\n  File
le/tmp/ansible-tmp-1539882988.6375449-224210235286945/AnsiballZ__k8s_raw.py\", line 48, in invoke_module\n    imp.load_module('__main__', mod, module, MOD_DESC)\n  File \"/usr/lib/python3.7/imp.py\", line 234, in load_m
ad_source(name, filename, file)\n  File \"/usr/lib/python3.7/imp.py\", line 169, in load_source\n    module = _exec(spec, sys.modules[name])\n  File \"<frozen importlib._bootstrap>\", line 630, in _exec\n  File \"<froze
ap_external>\", line 728, in exec_module\n  File \"<frozen importlib._bootstrap>\", line 219, in _call_with_frames_removed\n  File \"/tmp/ansible_k8s_raw_payload_0i0wsmue/__main__.py\", line 171, in <module>\n  File \"/
payload_0i0wsmue/__main__.py\", line 167, in main\n  File \"/tmp/ansible_k8s_raw_payload_0i0wsmue/ansible_k8s_raw_payload.zip/ansible/module_utils/k8s/raw.py\", line 91, in execute_module\nAttributeError: 'NoneType' obj
 'get'\n", "module_stdout": "", "msg": "MODULE FAILURE\nSee stdout/stderr for the exact error", "rc": 1}
        to retry, use: --limit @/home/manzo/workspace/untracked/k8s_raw_cert_manager_ansible_bug_report/k8s_raw_cert_manager_ansible_bug.retry

PLAY RECAP ****************************************************************************************************************************************************************************************************************
127.0.0.1                  : ok=2    changed=1    unreachable=0    failed=1
```

### Expected output

```
manzo@EVA ..8s_raw_cert_manager_ansible_bug_report (git)-[master] % ansible-playbook k8s_raw_cert_manager_ansible_bug.yml
 [WARNING]: Unable to parse /etc/ansible/hosts as an inventory source

 [WARNING]: No inventory was parsed, only implicit localhost is available

 [WARNING]: provided hosts list is empty, only localhost is available. Note that the implicit localhost does not match 'all'


PLAY [127.0.0.1] **********************************************************************************************************************************************************************************************************

TASK [Gathering Facts] ****************************************************************************************************************************************************************************************************
ok: [127.0.0.1]

TASK [k8s_raw] ************************************************************************************************************************************************************************************************************
changed: [127.0.0.1]

TASK [k8s_raw] ************************************************************************************************************************************************************************************************************
changed: [127.0.0.1]

PLAY RECAP ****************************************************************************************************************************************************************************************************************
127.0.0.1                  : ok=2    changed=2    unreachable=0    failed=0
```
